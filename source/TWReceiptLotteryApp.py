﻿# -*- coding: utf-8 -*-

import os
import re
import wx
import json
from AppInfo import AppInfo
from Prizes import Prize

try:
    from urllib.request import Request
    from urllib.request import urlopen
except ImportError:
    from urllib2 import Request
    from urllib2 import urlopen

class Frame(wx.Frame):
    def __init__(self, parent, id, title):
        self.MatchingCount = 0
        self.MatchedCount = 0

        wx.Frame.__init__(self, parent, id, title, style=(wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX))
        panel = wx.Panel(self)

        self.lblPhase = wx.StaticText(panel, label=u'期數:', pos=(10, 10));
        self.lblSpceialPrizeNumbers = wx.StaticText(panel, label=u'特別獎:', pos=(10, 30));
        self.lblGrandPrizeNumbers = wx.StaticText(panel, label=u'特獎:', pos=(10, 50));
        self.lblFirstPrizeNumbers = wx.StaticText(panel, label=u'頭獎:', pos=(10, 70));
        self.lblAdditionalNumbers = wx.StaticText(panel, label=u'六獎:', pos=(10, 90));
        
        self.OpenFileButton = wx.Button(panel, label=u'載入中獎號碼', pos=(275, 10), size=(100, 30))
        self.OpenFileButton.Bind(wx.EVT_BUTTON, self.__on_open_file)

        self.LoadWinningNumberButton = wx.Button(panel, label=u'下載中獎號碼', pos=(275, 50), size=(100, 30))
        self.LoadWinningNumberButton.Bind(wx.EVT_BUTTON, self.__on_load_winning_number)

        self.ThreeNumber = wx.TextCtrl(panel, -1, value='', pos=(10, 120), size=(50, -1), style=wx.TE_PROCESS_ENTER)
        self.ThreeNumber.Enable(False)
        self.ThreeNumber.SetMaxLength(3)
        self.ThreeNumber.Bind(wx.EVT_CHAR, self.__on_key_press)
        self.ThreeNumber.Bind(wx.EVT_KEY_UP, self.__on_enter)

        self.Summary = wx.StaticText(panel, label=u'結果:', pos=(70, 120));

        self.Result = wx.StaticText(panel, label='', pos=(10, 150));
        self.Result.SetFont(wx.Font(16, wx.DEFAULT, wx.NORMAL, wx.BOLD))
        
    def __load_file(self, path):
        try:
            with open(path, 'r', encoding='utf-8') as f:
                data = json.load(f)
        except json.JSONDecodeError as err:
            msg_dialog = wx.MessageDialog(None, 'Unknow format.', 'Error', wx.OK)
            msg_dialog.ShowModal()
            msg_dialog.Destroy()
            return
            
        self.SpecialPrizeNumbers    = [value for value in [value.strip() for value in data['special']] if len(value) != 0]
        self.GrandPrizeNumbers      = [value for value in [value.strip() for value in data['grand']] if len(value) != 0]
        self.FirstPrizeNumbers      = [value for value in [value.strip() for value in data['first']] if len(value) != 0]
        self.AdditionalPrizeNumbers = [value for value in [value.strip() for value in data['additional']] if len(value) != 0]
        self.lblPhase.SetLabel(u'期數:' + data['phase'])
        self.lblSpceialPrizeNumbers.SetLabel(u'特別獎:' + ', '.join(self.SpecialPrizeNumbers));
        self.lblGrandPrizeNumbers.SetLabel(u'特獎:' + ', '.join(self.GrandPrizeNumbers));
        self.lblFirstPrizeNumbers.SetLabel(u'頭獎:' + ', '.join(self.FirstPrizeNumbers));
        self.lblAdditionalNumbers.SetLabel(u'六獎:' + ', '.join(self.AdditionalPrizeNumbers));
        self.ThreeNumber.Enable(True)
    
    def __on_open_file(self, event):
        dirname = ''
        dialog = wx.FileDialog(self, 'Choose a file', dirname, '', '*.*', wx.FD_OPEN)
        if dialog.ShowModal() == wx.ID_OK:
            filename = dialog.GetFilename()
            dirname = dialog.GetDirectory()
            path = os.path.join(dirname, filename)
            self.__load_file(path)
        dialog.Destroy()
    
    def __on_load_winning_number(self, event):
        #laod winning number page list
        req = Request('http://service.etax.nat.gov.tw/etwmain/front/ETW183W1', 
                              headers={ 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' })
        with urlopen(req) as response:
            html = response.read().decode()
        links = re.findall('<a\shref="(.+ETW183W2.+)"\stitle=".+">(.+)</a>', html)
        #    url = link[0] #url of winning number page
        #    name = link[1] #name of winning number page
        link_names = [link[1] for link in links]
        
        dialog = wx.SingleChoiceDialog(None, u'選擇要下載的月份', u'下載中獎號碼', link_names)
        if dialog.ShowModal() == wx.ID_OK:
            selected = dialog.GetStringSelection()
            for link in links:
                if link[1] == selected:
                    save_dialog = wx.FileDialog(None, 'Save a file', '', '','*.xml', wx.FD_SAVE)
                    if save_dialog.ShowModal() == wx.ID_OK:
                        path = save_dialog.GetPath()
                        self.lblAdditionalNumbers.SetLabel(path)
                        #load selected winning number page
                        req = Request('http://service.etax.nat.gov.tw' + link[0],
                                            headers={ 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' })
                        with urlopen(req) as response:
                            html = response.read().decode()
                        match_title      = re.search('(<h4>\s*)(?P<data>.+)(?=\s*</h4>\s*.+\s*summary="統一發票中獎號碼")', html);
                        match_special    = re.search('(特別獎</th>\s*.+>\s*.+>)(?P<data>.+)(?=\s*</span>)', html);
                        match_grand      = re.search('(特獎</th>\s*.+>\s*.+>)(?P<data>.+)(?=\s*</span>)', html);
                        match_first      = re.search('(頭獎</th>\s*.+>\s*.+>)(?P<data>.+)(?=\s*</span>)', html);
                        match_additional = re.search('(增開六獎</th>\s*.+>\s*.+>)(?P<data>.+)(?=\s*</span>)', html);
                        
                        if match_title is None or match_special is None \
                                or match_grand is None or match_first is None \
                                or match_additional is None:
                            #fail to extract winning from html
                            msg_dialog = wx.MessageDialog(None, 'Something wrong.', 'Error', wx.OK)
                            msg_dialog.ShowModal()
                            msg_dialog.Destroy()
                            return
                        
                        title      = match_title.group('data').replace('、',',')
                        special    = match_special.group('data').replace('、',',').split(',')
                        grand      = match_grand.group('data').replace('、',',').split(',')
                        first      = match_first.group('data').replace('、',',').split(',')
                        additional = match_additional.group('data').replace('、',',').split(',')
                        with open(path, 'w', encoding='utf-8') as f:
                            data = {
                                    'version': '2.0',
                                    'phase': title,
                                    'special': special,
                                    'grand': grand,
                                    'first': first,
                                    'additional': additional,
                            }
                            json.dump(data, f, ensure_ascii=False, indent=4)
                        self.__load_file(path)
                    
                    save_dialog.Destroy()
                    break
        
        dialog.Destroy()
        
    def __on_key_press(self, event):
        keycode = event.GetKeyCode()
        if keycode == 27:
            self.ThreeNumber.Clear()
        elif keycode in [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57]:
            event.Skip()
    
    def __on_enter(self, event):
        user_input = self.ThreeNumber.GetValue()
        if len(user_input) == 3:
            prize = Prize(self.SpecialPrizeNumbers, self.GrandPrizeNumbers, self.FirstPrizeNumbers, self.AdditionalPrizeNumbers)
            isWin = False
            resultString = ''
            if prize.is_it_win_additional_prize(user_input):
                isWin = True
                resultString = u'你中了六獎200元，六獎號碼是: %s'%', '.join(self.AdditionalPrizeNumbers)
            if prize.is_it_win_regular_prize(user_input):
                if isWin:
                    resultString += u'\r\n'
                isWin = True
                resultString += u'你中了六獎200元\r\n請再對頭獎前5碼，末4碼獎金1千，末5碼獎金4千，末6碼獎金1萬，末7碼獎金4千，末8碼獎金20萬\r\n頭獎號碼是: %s'%', '.join(self.FirstPrizeNumbers)
            if prize.is_it_win_grand_prize(user_input):
                if isWin:
                    resultString += u'\r\n'
                isWin = True
                resultString += u'你可能中大獎了，請再對特獎前5碼，未8碼獎金200萬元\r\n特獎號碼是: %s'%', '.join(self.GrandPrizeNumbers)
            if prize.is_it_win_spceial_prize(user_input):
                if isWin:
                    resultString += u'\r\n'
                isWin = True
                resultString += u'你可能中大獎了，請再對特別獎前5碼，未8碼獎金1000萬元\r\n特別獎號碼是: %s'%', '.join(self.SpecialPrizeNumbers)
            
            self.MatchingCount+=1
            if isWin:
                self.MatchedCount+=1
                self.Result.SetForegroundColour((255,255,255))
                self.Result.SetBackgroundColour((255,0,0))
                self.Result.SetLabel(u'中獎了!!!?')
            else:
                resultString = u'沒中獎'
                self.Result.SetForegroundColour((0,0,0))
                self.Result.SetBackgroundColour((200,200,200))
                self.Result.SetLabel(resultString)
            self.Summary.SetLabel(u'結果: %s%s，共比對了%s張，中獎%d張，沒中%d張'%(user_input, (u'中獎了' if isWin else u'沒中獎'), self.MatchingCount, self.MatchedCount, self.MatchingCount - self.MatchedCount))
            self.ThreeNumber.Clear()

            if isWin:
                wx.MessageBox(resultString, u'中獎了!!!?', wx.OK | wx.ICON_WARNING)


class TWReceiptLotteryApp(wx.App):
    
    def __init__(self, redirect=True, filename=None):
        wx.App.__init__(self, redirect, filename)
    
    def OnInit(self):
        self.AppInfo = AppInfo;
        self.frame = Frame(parent=None, id=wx.ID_ANY, title='%(Title)s - ver:%(Version)s'%self.AppInfo)
        self.frame.Show()
        self.SetTopWindow(self.frame)
        return True
