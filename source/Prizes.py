# -*- coding: utf-8 -*-

import re

class Prize(object):
    def __init__(self, spceialPrizeNumbers, grandPrizeNumbers, firstPrizeNumbers, additionalPrizeNumbers):
        self.SpceialPrizeNumbers    = spceialPrizeNumbers
        self.GrandPrizeNumbers      = grandPrizeNumbers
        self.FirstPrizeNumbers      = firstPrizeNumbers
        self.AdditionalPrizeNumbers = additionalPrizeNumbers

    def is_it_win_spceial_prize(self, number):
        """特獎 - 同期統一發票收執聯末8 位數號碼與特別獎中獎號碼末8 位相同者各得獎金1000 萬元"""
        return self.__is_it_win(self.SpceialPrizeNumbers, number)
    
    def is_it_win_grand_prize(self, number):
        """特獎 - 同期統一發票收執聯末8 位數號碼與特獎中獎號碼末8 位相同者各得獎金200 萬元"""
        return self.__is_it_win(self.GrandPrizeNumbers, number)
    
    def is_it_win_regular_prize(self, number):
        """頭獎 - 同期統一發票收執聯末8 位數號碼與頭獎中獎號碼末8 位相同者各得獎金20 萬元
            二獎 - 同期統一發票收執聯末7 位數號碼與頭獎中獎號碼末7 位相同者各得獎金4 萬元
            三獎 - 同期統一發票收執聯末6 位數號碼與頭獎中獎號碼末6 位相同者各得獎金1 萬元
            四獎 - 同期統一發票收執聯末5 位數號碼與頭獎中獎號碼末5 位相同者各得獎金4 千元
            五獎 - 同期統一發票收執聯末4 位數號碼與頭獎中獎號碼末4 位相同者各得獎金1 千元
            六獎 - 同期統一發票收執聯末3 位數號碼與頭獎中獎號碼末3 位相同者各得獎金2 百元"""
        return self.__is_it_win(self.FirstPrizeNumbers, number)
    
    def is_it_win_additional_prize(self, number):
        """增開六獎 - 同期統一發票收執聯末3 位數號碼與上列號碼相同者各得獎金2 百元"""
        return self.__is_it_win(self.AdditionalPrizeNumbers, number)
    
    def __is_it_win(self, prizeNumbers, number):
        result = False
        for n in prizeNumbers:
            result = n[-3:] == number
            if result:
                break
        return result
