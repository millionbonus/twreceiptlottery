﻿# -*- coding: utf-8 -*-

AppInfo = {
    'Title': 'TWReceiptLottery',
    'Description':'TWReceiptLottery is a tiny utility that can help users to know whether they have won a Taiwan receipt lottery.',
    'Company': '',
    'Copyright': '',
    'Version': '1.1.0.0'
}
